import React, { useState, useEffect } from "react";
import { MapContainer, TileLayer, Marker, Popup } from 'react-leaflet'
import Axios from 'axios'
import * as smartmileLocations from "./data/smartmile-locations.json";
import mapStyles from "./mapStyles";
import MapWrapper from "./MapWrapper";

function Map() {
  const [dataresponse, setDataResponse] = useState({});
  const [selectedPark, setSelectedPark] = useState(null);


  //Calling an endpoint to get all locations
  const  getLocation = () => {
    Axios.get("https://api.smartmileos.com/public/locations",
    {
      headers: { "x-smartmile-app-key": "73701487-d158-4d39-877e-a84bca984765"}
    }).then((response) => {
      setDataResponse(response.data)
    })
  }
   
  //getting locations
  useEffect(() => {
    getLocation()
    console.log(dataresponse)
  }, [])

  //creating marks function for the map
  const marks = (data) =>{
    return (
      data.map((park) => (
          <Marker
          key={park.id}
          position={{
            lat: park.apt.UniAddress.latitude,
            lng: park.apt.UniAddress.longitude,
          }}
        />
      ))
    )
  }

  return (
    <MapContainer center={[63.2467777, 25.9209164]} zoom={5.5} scrollWheelZoom={true} style={{width: '100vw', height: '100vh'}}>
      <TileLayer
        attribution='&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
        url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
      />
      <MapWrapper />
      {dataresponse.locations && marks(dataresponse.locations)}
      {!dataresponse && marks(smartmileLocations.locations)}
    </MapContainer>
  )
}

export default Map